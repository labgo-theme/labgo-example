---
layout: publications
title: Publications
description: "The publications of The Dean Lab"
menu:
  navbar:
    weight: 6
publications:
  - publication_title: Some cool work
    authors: Brendan Murphy
    journal: cool journal
    year: "2021"
    link: www.brendanmurphy.xyz
---
